CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
JQuery File Upload module gives a fancy look for uploading file. It gives a cck file widget.

REQUIREMENTS
------------
This module requires the following module:
 * JQuery Update Module (https://drupal.org/project/jquery_update)

INSTALLATION
------------
 * Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7 for further information.
 * You likely want to disable Toolbar module, since its output clashes with
  Administration menu.

CONFIGURATION
-------------
 * Configure JQuery Update module in Administration » Configuration » Development:
  Set jQuery Version: 1.7

Current maintainers:
 * Safique Ahmed Faruque - https://drupal.org/user/1256508
