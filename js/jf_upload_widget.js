(function ($) {
    function JFUPLOAD(id, settings){
        var ref = {};
        ref.settings = {};
        ref.id = null;
        ref.upload_element_obj;
        ref.instance = {};
        ref.init = function() {
            ref.settings = settings;
            ref.upload_element_obj = $('#' + ref.settings.element_id);
            var pattern = new RegExp("(\.|\/)(" + ref.settings.file_types + ")$");
            ref.upload_element_obj.fileupload({
                url: ref.settings.upload_url,
                sequentialUploads: true,
                uploadTemplateId: ref.settings.upload_template_id,
                downloadTemplateId: ref.settings.download_template_id,
                acceptFileTypes: pattern,
                previewMaxWidth: 120,
                maxFileSize: 1024000,
                autoUpload: true,
                maxNumberOfFiles: ref.settings.file_upload_limit,
                formData:{
                    file_path: ref.settings.post_params.file_path,
                    field_name: ref.settings.post_params.field_name,
                    element_name: ref.settings.post_params.element_name,
                    field_container: '#' + ref.settings.element_id,
                    instance_settings:ref.settings.post_params.instance_settings
                },
                done: function (e, data) {
                    var that = $(this).data('blueimp-fileupload') ||
                    $(this).data('fileupload'),
                    files = that._getFilesFromResponse(data),
                    template,
                    deferred;
                    if (data.context) {
                        data.context.each(function (index) {
                            var file = files[index] ||
                            {
                                error: 'Empty file upload result'
                            },
                            deferred = that._addFinishedDeferreds();
                            if (file.error) {
                                that._adjustMaxNumberOfFiles(1);
                            }
                            
                            that._transition($(this)).done(
                                function () {
                                    var node = $(this);
                                    template = that._renderDownload([file])
                                    .replaceAll(node);
                                    that._forceReflow(template);
                                    that._transition(template).done(
                                        function () {
                                            data.context = $(this);
                                            that._trigger('completed', e, data);
                                            that._trigger('finished', e, data);
                                            deferred.resolve();
                                        }
                                        );
                                    ref.createFileField(file,template);
                                }
                                );
                        });
                    } else {
                        if (files.length) {
                            $.each(files, function (index, file) {
                                if (data.maxNumberOfFilesAdjusted && file.error) {
                                    that._adjustMaxNumberOfFiles(1);
                                } else if (!data.maxNumberOfFilesAdjusted &&
                                    !file.error) {
                                    that._adjustMaxNumberOfFiles(-1);
                                }
                            });
                            data.maxNumberOfFilesAdjusted = true;
                        }
                        template = that._renderDownload(files)
                        .appendTo(that.options.filesContainer);
                        that._forceReflow(template);
                        deferred = that._addFinishedDeferreds();
                        that._transition(template).done(
                            function () {
                                data.context = $(this);
                                that._trigger('completed', e, data);
                                that._trigger('finished', e, data);
                                deferred.resolve();
                            }
                            );
                        $(files).each(function(index, file){
                            ref.createFileField(file,$(template[index]));
                                    
                        })
                    }
                },
                // Callback for file deletion:
                destroyed: function (e, data) {
                    $('input[name="'+data.element_name+'"]').remove();
                }
            });
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: ref.upload_element_obj.fileupload('option', 'url'),
                dataType: 'json',
                type: 'POST',
                data:{
                    load_existing_files: ref.settings.existing_file_ids,
                    file_path: ref.settings.post_params.file_path,
                    field_name: ref.settings.post_params.field_name,
                    element_name: ref.settings.post_params.element_name,
                    field_container: '#' + ref.settings.element_id,
                    instance_settings:ref.settings.post_params.instance_settings  
                },
                context: ref.upload_element_obj
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                .call(this, null, {
                    result: result
                });
            });
        }   
        ref.createFileField = function(file, template){
            var length = $('input[name^="'+file.element_name+'"]',$(file.field_container)).length;
            var name = file.element_name+'['+length+'][fid]';
            var element = ref.createElement('hidden',name,file.fid);
            $(element).attr('id','jf-upload-file-'+file.fid)
            .appendTo($(file.field_container));
            $('.delete button',template).data('fid',file.fid)
            .data('element_name',name);
        } 
        ref.createElement = function(type, name, value, attributes){
            var inputElement = '';
            if(type == undefined){
                type = 'text';
            }
            if(value != undefined){
                value = ' value="'+value+'" ';
            }
            if(name != undefined){
                name = ' name="'+name+'" ';
            }
            if(attributes == undefined){
                attributes = ' ';
            }
            if(type){
                inputElement += '<input type="'+type+'"'+name+value+attributes+'/>';
            }
            return inputElement;
        }
        return ref;
    }
    $(function() {
        if (Drupal.settings.jf_settings) {
            Drupal.sffu = {};
            var settings = Drupal.settings.jf_settings;
            for (var id in settings) {
                Drupal.sffu[id] = new JFUPLOAD(id, settings[id]);
                Drupal.sffu[id].init();
            };
        };
    });
})(jQuery);
